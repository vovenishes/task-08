package com.epam.rd.java.basic.task8.comparators;

import java.util.Comparator;

import com.epam.rd.java.basic.task8.Flower;

public class FlowersComparatorBySoil implements Comparator<Flower>{

	@Override
	public int compare(Flower o1, Flower o2) {
		return o1.getSoil().compareTo(o2.getSoil());
	}
}
