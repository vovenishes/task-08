package com.epam.rd.java.basic.task8.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Util;
import com.epam.rd.java.basic.task8.VisualParametrs;
import com.epam.rd.java.basic.task8.constant.XMLconstants;
import com.epam.rd.java.basic.task8.constant.XMLflowerConstant;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private static boolean isName;
	private static boolean isSoil;
	private static boolean isOrigin;
	private static boolean isVisualParametrs;
	private static boolean isStemColour;
	private static boolean isLeafColour;
	private static boolean isAveLenFlower;
	private static boolean isGrowingTips;
	private static boolean isTempreture;
	private static boolean isLighting;
	private static boolean isWatering;
	private static boolean isMultiplying;
	
	private String xmlFileName;
	private List<Flower> flowers;
	private Flower flower;
	private VisualParametrs visualParametrs;
	private GrowingTips growingTips;
	private String lightningAtt;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
	}
	
	public List<Flower> getFlowers() {
		return flowers;
	}

	public void parseXML(boolean validate) {
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		
		if(validate) {
			factory.setValidating(true);
			try {
				factory.setFeature(XMLconstants.FEATURE_TURN_VALIDATION_ON, true);
				factory.setFeature(XMLconstants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
			} catch (SAXNotRecognizedException | SAXNotSupportedException | ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
		
		SAXParser parser;
		
			try {
				parser = factory.newSAXParser();
				parser.parse(xmlFileName, this);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		
		if(qName.equals(XMLflowerConstant.FLOWER)) {
			flower = new Flower();
		} else if(localName.equals(XMLflowerConstant.FLOWER_NAME)) {
			isName = true;
		} else if(localName.equals(XMLflowerConstant.FLOWER_SOIL)) {
			isSoil = true;
		} else if(localName.equals(XMLflowerConstant.FLOWER_ORIGIN)) {
			isOrigin = true;
		} else if(localName.equals(XMLflowerConstant.VISUAL_PARAMETRS)) {
			visualParametrs = new VisualParametrs();
			isVisualParametrs = true;
		} else if(localName.equals(XMLflowerConstant.VP_STEM_COLOUR)) {
			isStemColour = true;
		} else if(localName.equals(XMLflowerConstant.VP_LEAF_COLOUR)) {
			isLeafColour = true;
		} else if(localName.equals(XMLflowerConstant.VP_AVE_LEN_FLOWER)) {
			isAveLenFlower = true;
		} else if(localName.equals(XMLflowerConstant.GROWING_TIPS)) {
			growingTips = new GrowingTips();
			isGrowingTips = true;
		} else if(localName.equals(XMLflowerConstant.GT_TEMPRETURE)) {
			isTempreture = true;
		} else if(localName.equals(XMLflowerConstant.GT_LIGHTING)) {
			growingTips.setLighting(attributes.getValue("lightRequiring"));
			isLighting = true;
		} else if(localName.equals(XMLflowerConstant.GT_WATERING)) {
			isWatering = true;
		} else if(localName.equals(XMLflowerConstant.MULTIPLYING)) {
			isMultiplying = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equals(XMLflowerConstant.FLOWER)) {
			if(!Objects.isNull(flower)) {
				flowers.add(flower);
			}
		
		} else if(localName.equals(XMLflowerConstant.FLOWER_NAME)) {
			isName = false;
		} else if(localName.equals(XMLflowerConstant.FLOWER_SOIL)) {
			isSoil = false;
		} else if(localName.equals(XMLflowerConstant.FLOWER_ORIGIN)) {
			isOrigin = false;
		} else if(localName.equals(XMLflowerConstant.VISUAL_PARAMETRS)) {
			flower.setVisualParameters(visualParametrs);
			isVisualParametrs = false;
		} else if(localName.equals(XMLflowerConstant.VP_STEM_COLOUR)) {
			isStemColour = false;
		} else if(localName.equals(XMLflowerConstant.VP_LEAF_COLOUR)) {
			isLeafColour = false;
		} else if(localName.equals(XMLflowerConstant.VP_AVE_LEN_FLOWER)) {
			isAveLenFlower = false;
		} else if(localName.equals(XMLflowerConstant.GROWING_TIPS)) {
			flower.setGrowingTips(growingTips);
			isGrowingTips = false;
		} else if(localName.equals(XMLflowerConstant.GT_TEMPRETURE)) {
			isTempreture = false;
			
		} else if(qName.equals(XMLflowerConstant.GT_LIGHTING)) {
			isLighting = false;
		} else if(qName.equals(XMLflowerConstant.GT_WATERING)) {
			isWatering = false;
		} else if(qName.equals(XMLflowerConstant.MULTIPLYING)) {
			isMultiplying = false;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length).trim();
		if(!value.isEmpty()) {
			if(isName) {
				flower.setName(value);
			} else if (isSoil) {
				flower.setSoil(value);
			} else if (isOrigin) {
				flower.setOrigin(value);
			} else if (isVisualParametrs) {
				if(!setVisualParametrs(value)) {
					visualParametrs = new VisualParametrs();
				}
			} else if (isGrowingTips) {
				if(!setGrowingTips(value)) {
					growingTips = new GrowingTips();
				}
			} else if (isMultiplying) {
				flower.setMultiplying(value);
			}
			
		}
	}
	
	private boolean setVisualParametrs(String s) {
		if(isStemColour) {
			visualParametrs.setStemColour(s);
			return true;
		} else if (isLeafColour) {
			visualParametrs.setLeafColour(s);
			return true;
		} else if (isAveLenFlower) {
			visualParametrs.setAveLenFlower(s);
			return true;
		}
		
		
		return false;
	}
	
	private boolean setGrowingTips(String s) {
		
		if(isTempreture) {
			growingTips.setTempreture(s);			
			return true;
			
		} else if (isLighting) {					
			return true;
		} else if (isWatering) {
			growingTips.setWatering(s);
			return true;
		}
		return false;
	}
	
	public void saveToXML(String url) {
		try {
			Util.writeSaxStax(flowers, url);
		} catch (IOException | XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
